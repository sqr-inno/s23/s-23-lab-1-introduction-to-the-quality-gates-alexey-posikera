FROM maven:3 AS builder

WORKDIR /app

COPY pom.xml /app
COPY src /app/src

RUN mvn clean package

FROM anapsix/alpine-java

COPY --from=builder /app/target/main-0.0.1-SNAPSHOT.jar /

EXPOSE 8080

ENTRYPOINT java -jar main-0.0.1-SNAPSHOT.jar
